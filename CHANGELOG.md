## [1.0.1](https://gitlab.com/rtgr-demos/bankback/compare/v1.0.0...v1.0.1) (2023-04-06)


### Bug Fixes

* cls1 ([5307f7c](https://gitlab.com/rtgr-demos/bankback/commit/5307f7c91ebb04c86ebaff12a397cfce1b7d3ffa))

# 1.0.0 (2023-03-24)


### Features

* Initial Load ([a0ba6c3](https://gitlab.com/rtgr-demos/bankback/commit/a0ba6c3517b452966427ddbfd0cd4988ce1377a6))

## [1.0.10](https://gitlab.com/cafat2/bankback/compare/v1.0.9...v1.0.10) (2023-03-24)


### Bug Fixes

* remove Dockerfile ([4451850](https://gitlab.com/cafat2/bankback/commit/44518500cc228cd512393c261ef05c486f297c41))

## [1.0.9](https://gitlab.com/cafat2/bankback/compare/v1.0.8...v1.0.9) (2023-03-24)


### Bug Fixes

* using mvwn ([f29e64a](https://gitlab.com/cafat2/bankback/commit/f29e64af1f958fa9f0db51fb70f0bd958ca44170))

## [1.0.8](https://gitlab.com/cafat2/bankback/compare/v1.0.7...v1.0.8) (2023-03-24)


### Bug Fixes

* using docker image ([7bc00b8](https://gitlab.com/cafat2/bankback/commit/7bc00b8d9b0de1d5593192cbf878a84db69d6e06))

## [1.0.7](https://gitlab.com/cafat2/bankback/compare/v1.0.6...v1.0.7) (2023-03-24)


### Bug Fixes

* typo ([156dc5d](https://gitlab.com/cafat2/bankback/commit/156dc5d707bed8f9079d9114c81d60402e3162f5))

## [1.0.6](https://gitlab.com/cafat2/bankback/compare/v1.0.5...v1.0.6) (2023-03-24)


### Bug Fixes

* typo ([dee9ec6](https://gitlab.com/cafat2/bankback/commit/dee9ec6a96933a57cefa0dcb2835e1064b2905dc))
* typo ([488fa14](https://gitlab.com/cafat2/bankback/commit/488fa1440a144429989e0b309673b2fbc16e057e))
* typo ([099b8bf](https://gitlab.com/cafat2/bankback/commit/099b8bf4a4a1557220534edfb3a5e081948444fc))
* typo ([7f7727b](https://gitlab.com/cafat2/bankback/commit/7f7727bace3929c77d14e3c24cfe98cd8094caaf))
* typo ([ad2a925](https://gitlab.com/cafat2/bankback/commit/ad2a92534a9eb91207dbf61bb41a664ae5741169))
* typo ([93a21e7](https://gitlab.com/cafat2/bankback/commit/93a21e77d5b54179abd7fb90fe6c856f512782f0))
* typo ([b24c955](https://gitlab.com/cafat2/bankback/commit/b24c955b388ff87481c23c620dd9b5916090e7fa))
* typo ([341201f](https://gitlab.com/cafat2/bankback/commit/341201f5bcbffd99860d497faf2739779f4c0409))
* typo ([3a899c6](https://gitlab.com/cafat2/bankback/commit/3a899c6095b61d12351e16265df6c56730530c4f))
* typo ([2ed852e](https://gitlab.com/cafat2/bankback/commit/2ed852e95b18f3b5aab3f03619572c46551b0d67))

## [1.0.5](https://gitlab.com/cafat2/bankback/compare/v1.0.4...v1.0.5) (2023-03-23)


### Bug Fixes

* ok ([0ff52d4](https://gitlab.com/cafat2/bankback/commit/0ff52d49f41f1002cfe70ed1492e771a6ab82663))

## [1.0.4](https://gitlab.com/cafat2/bankback/compare/v1.0.3...v1.0.4) (2023-03-23)


### Bug Fixes

* ok ([f83b8dd](https://gitlab.com/cafat2/bankback/commit/f83b8dd82ab39608c2695e7e7b14eb3654b9ac50))

## [1.0.3](https://gitlab.com/cafat2/bankback/compare/v1.0.2...v1.0.3) (2023-03-23)


### Bug Fixes

* debug ([67b387d](https://gitlab.com/cafat2/bankback/commit/67b387d4a6a210fadcf366e83ee16d8818f3fd26))

## [1.0.2](https://gitlab.com/cafat2/bankback/compare/v1.0.1...v1.0.2) (2023-03-23)


### Bug Fixes

* debug ([51ec052](https://gitlab.com/cafat2/bankback/commit/51ec052472276ecd43878024b96bf5f3b592f641))

## [1.0.1](https://gitlab.com/cafat2/bankback/compare/v1.0.0...v1.0.1) (2023-03-23)


### Bug Fixes

* ci ([3474e07](https://gitlab.com/cafat2/bankback/commit/3474e078c2174676a0afa549c1ca5cd60423e8b9))

# 1.0.0 (2023-03-23)


### Bug Fixes

* ignore folder ([85fe553](https://gitlab.com/cafat2/bankback/commit/85fe553423e6bbf9624671c4cc0f583c38340110))
* implement rest api ([caa3793](https://gitlab.com/cafat2/bankback/commit/caa3793f5766e5cb002d3962870ac88b0ee659c6))
* Refactoring code ([3014780](https://gitlab.com/cafat2/bankback/commit/3014780e9936236bf909e9de88772866eded3f22))
* status ([90e7e09](https://gitlab.com/cafat2/bankback/commit/90e7e09eb041154dc6bb78a16cdee877808210e9))
* typo ([f952ac3](https://gitlab.com/cafat2/bankback/commit/f952ac3f34790feb0bf0ac271359bc7adc1f54a5))
