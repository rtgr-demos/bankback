package com.retengr.bankback.dao.repository;

import com.retengr.bankback.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {

    List<Client> findByNom(String name);
    List<Client> findByNomStartingWith(String prefix);

}
