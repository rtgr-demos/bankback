package com.retengr.bankback.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double solde;
    private String numero;
    private String description;

    public Compte() {
    }

    public Compte(double solde, String numero, String description) {
        this.id = id;
        this.description = description;
        this.numero = numero;
        this.solde = solde;
    }

    public Compte(Long id, double solde, String numero, String description) {
        this.id = id;
        this.description = description;
        this.numero = numero;
        this.solde = solde;
    }

    public void credit(double m) {
        solde = solde + m;
    }

    public void debit(double m) {
        solde = solde - m;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public void credit(int m) {solde+=m;}
    public void debit(int m) {solde-=m;}
}
