package com.retengr.bankback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Arrays;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan("com.retengr.bankback.model")
public class BankbackApplication implements CommandLineRunner {

	@Autowired
	ApplicationContext appContext;

	public static void main(String[] args) {

		SpringApplication.run(BankbackApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		String[] beans = appContext.getBeanDefinitionNames();
		Arrays.sort(beans);
		for (String bean : beans) {
			// System.out.println(bean);
		}

		System.out.println("\nSpringBoot a trouvé "
				+ beans.length
				+ " beans auto-configurés dans l'environnement d'exécution \n\n");

	}
}
