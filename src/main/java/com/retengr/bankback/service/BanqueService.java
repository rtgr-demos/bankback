package com.retengr.bankback.service;

import com.retengr.bankback.dao.repository.ClientRepository;
import com.retengr.bankback.dao.repository.CompteRepository;
import com.retengr.bankback.model.Client;
import com.retengr.bankback.model.Compte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class BanqueService {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CompteRepository compteRepository;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public Optional<Client> detailClient(Long id) {
        return clientRepository.findById(id);
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public List<Client> getClients() {
        System.out.println("hello");
        return clientRepository.findAll();
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public List<Compte> getComptes() {
        return compteRepository.findAll();
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void ajouteClient(Client c) {
        clientRepository.save(c);
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void ajouteCompte(Compte c) {
        compteRepository.save(c);
    }

    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void transfer(Long id1, Long id2, int montant) throws Exception {
        System.out.println("transfering");
        Optional<Compte> c1 = compteRepository.findById(id1);
        Optional<Compte> c2 = compteRepository.findById(id2);
        c1.get().debit(montant);
        c2.get().credit(montant);
    }
}
