package com.retengr.bankback.rest;

import com.retengr.bankback.model.Compte;
import com.retengr.bankback.service.BanqueService;
import com.retengr.bankback.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BanqueWeb {

    @Autowired
    private BanqueService banqueService;


    @GetMapping("/client")
    public List<Client> findAllClients(){
        return banqueService.getClients();
    }

    @GetMapping("/compte")
    public List<Compte> findAllComptes(){
        return banqueService.getComptes();
    }

    @GetMapping("/transfer/{idSource}/{idDest}/{amount}")
    public ResponseEntity<?> transfer(@PathVariable Long idSource, @PathVariable Long idDest, @PathVariable int amount){
        try {
            banqueService.transfer(idSource,idDest,amount);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
