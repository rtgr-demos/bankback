package com.retengr.bankback;

import com.retengr.bankback.model.Client;
import com.retengr.bankback.model.Compte;
import com.retengr.bankback.service.BanqueService;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@SpringBootTest
@EntityScan("com.retengr.model")
class BankBackApplicationTests {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private BanqueService service;


    @Autowired
    private EntityManagerFactory emf;

    @Test
    void contextLoads() {
    }


    @Test
    @Order(1)
    @Transactional(Transactional.TxType.REQUIRES_NEW)

    void saveClient() {
		Client c = new Client();
		c.setNom("toto");
		c.setPrenom("titi");
        em.persist(c);
    }

    @Test
    @Order(2)
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void listClient() {
        List<Client> clients = em.createQuery("select c from Client c").getResultList();

        for (Client client: clients) {
            System.out.println(client.getNom());
        }
    }



    @BeforeTestMethod
    void initDB() {

        em.persist(new Compte(1000, "010101", "desc1"));
        em.persist(new Compte(1000, "010101", "desc1"));

    }

    @Test
    void transfer() {
    }
}
